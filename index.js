//console.log(`Zawarudo!`)


// Add Post Data

	let posts = [];
	let count = 1; // for Mongo-database ID

	document.querySelector("#form-add-post").addEventListener("submit", (event) => {
		event.preventDefault();
		posts.push({
			id: count,
			title: document.querySelector("#txt-title").value,
        	body: document.querySelector("#txt-body").value
		})
		count++;
		showPosts(posts);
	});


// Show posts
	const showPosts = (posts) => {
		let postEntries = ``;

		posts.forEach((newPost) => {
			postEntries += `
			<div id = "post-${newPost.id}">
				<h4 id = "post-title-${newPost.id}">${newPost.title}</h4>
				<p id = "post-body-${newPost.id}">${newPost.body}</p>
				<button onclick = "editPost('${newPost.id}')">Edit</button>
				<button onclick = "deletePost('${newPost.id}')">Delete</button>
			</div>
			`
		})
		
		document.querySelector("#HERE").innerHTML = postEntries;
	}


// Edit posts
	const editPost = (id) => {
		let title = document.querySelector(`#post-title-${id}`).innerHTML;
		let body = document.querySelector(`#post-body-${id}`).innerHTML;

		console.log(document.querySelector(`#txt-edit-title`).value)
		console.log(document.querySelector(`#txt-edit-body`).value)

		document.querySelector(`#txt-edit-title`).value = title;
		document.querySelector(`#txt-edit-body`).value = body;
		document.querySelector(`#txt-edit-id`).value = id;

	}

	document.querySelector("#form-edit-post").addEventListener("submit", (event) => {
		event.preventDefault();

		const idTobeUpdated = document.querySelector("#txt-edit-id").value;

		for(let i=0;i < posts.length;i++){
			if(posts[i].id.toString() === idTobeUpdated){
				posts[i].title = document.querySelector("#txt-edit-title").value
				posts[i].body = document.querySelector("#txt-edit-body").value
				showPosts(posts);
				alert("Succes!");
				break;
			}
		}
	});


// Delete posts
	const deletePost = (id) => {
		posts.splice(Number(id) - 1, 1);
		document.querySelector(`#post-${id}`).remove();
	}

	

	
	// for(let i = 1; i < posts.length+1; i++){
	// 	if(del == i){
	// 		posts.splice(del, 1);

	// 		showPosts(posts);
	// 		alert("Succes!");
	// 		break;
	// 	}
	// }



	// const here = document.querySelector("#fullName");
	// const changeColor = () => here.style.color = document.getElementById("color").value;
	// const addPost = document.querySelector("#fullName");